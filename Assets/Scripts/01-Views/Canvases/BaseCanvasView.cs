﻿using UnityEngine;
using Pong.Controllers;

namespace Pong.Views
{
    /// <summary>
    /// Base canvas view component.
    /// </summary>
    [DisallowMultipleComponent]
    public class BaseCanvasView : MonoBehaviour, IDisplayable
    {
        /// <summary>
        /// The game controller witch controls this component.
        /// </summary>
        public GameMatchController GameController { get; internal set; }

        private void Reset() => FindComponents();

        /// <summary>
        /// Shows the view.
        /// </summary>
        public void Show() => gameObject.SetActive(true);

        /// <summary>
        /// Hides the view.
        /// </summary>
        public void Hide() => gameObject.SetActive(false);

        protected virtual void FindComponents() { }
    }
}
