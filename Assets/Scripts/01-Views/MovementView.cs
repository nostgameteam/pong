﻿using UnityEngine;
using Pong.Models;
using Pong.Controllers;

namespace Pong.Views
{
    /// <summary>
    /// View component for <see cref="MovementController"/>.
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(BoxCollider2D))]
    public sealed class MovementView : MonoBehaviour, IPositionable, IMoveable, IPauseable
    {
        [Tooltip("The move model data used to feed the controller.")]
        public MovementModel model;

        /// <summary>
        /// Controller used to move and detect collisions.
        /// </summary>
        public MovementController Controller { get; private set; }

        /// <summary>
        /// The current position.
        /// </summary>
        public Vector2 Position => transform.position;

        /// <summary>
        /// The current top right most position.
        /// </summary>
        public Vector2 TopRightPosition => Position + Controller.HalfSize;

        /// <summary>
        /// The current bottom left most position.
        /// </summary>
        public Vector2 BottomLeftPosition => Position - Controller.HalfSize;

        private Vector2 startPosition;
        private BoxCollider2D boxCollider;

        private void Awake()
        {
            SetupRequiredComponents();
            InitializeController();
            startPosition = transform.position;
        }

        private void FixedUpdate()
        {
            Controller.UpdatePosition(transform.position, model.TotalSpeed, model.Collision);
            transform.position = Controller.Position;
        }

        /// <summary>
        /// Moves this object using the given direction.
        /// </summary>
        /// <param name="direction">A normalized direction.</param>
        public void Move(Vector2 direction) => Controller.Direction = direction;

        /// <summary>
        /// Resets position to start.
        /// </summary>
        public void ResetPosition() => transform.position = startPosition;

        /// <summary>
        /// Pauses the movement.
        /// </summary>
        public void Pause() => enabled = false;

        /// <summary>
        /// Resumes the movement.
        /// </summary>
        public void Resume() => enabled = true;

        private void InitializeController()
        {
            if (model == null) throw new System.NullReferenceException("Model was not set.");
            Controller = new MovementController(boxCollider.bounds, model.InitialDirection);
        }

        private void SetupRequiredComponents() => boxCollider = GetComponent<BoxCollider2D>();
    }
}
