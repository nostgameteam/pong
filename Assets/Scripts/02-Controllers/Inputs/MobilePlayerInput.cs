﻿using UnityEngine;

namespace Pong.Controllers
{
    /// <summary>
    /// Input controller for mobile input
    /// </summary>
    [DisallowMultipleComponent]
    public sealed class MobilePlayerInput : MonoBehaviour, IDisplayable
    {
        private bool hasPlayerInput;
        private PlayerInput playerInput;

        private void Start() => FindPlayerInput();

        public void MoveUp()
        {
            if (hasPlayerInput) playerInput.Move(Vector2.up);
        }

        public void StopMove()
        {
            if (hasPlayerInput) playerInput.Move(Vector2.zero);
        }

        public void MoveDown()
        {
            if (hasPlayerInput) playerInput.Move(Vector2.down);
        }

        public void Show() => gameObject.SetActive(true);

        public void Hide() => gameObject.SetActive(false);

        private void FindPlayerInput()
        {
            playerInput = FindObjectOfType<PlayerInput>();
            hasPlayerInput = playerInput != null;
        }
    }
}
