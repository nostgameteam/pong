﻿using UnityEngine;

namespace Pong.Controllers
{
    /// <summary>
    /// Interface used on objects able to move.
    /// </summary>
    public interface IMoveable
    {
        /// <summary>
        /// Moves the object using the given direction.
        /// </summary>
        /// <param name="direction">A normalized direction.</param>
    	void Move(Vector2 direction);
    }
}