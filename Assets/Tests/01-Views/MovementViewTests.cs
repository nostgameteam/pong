﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using Pong.Models;
using Pong.Views;

namespace Pong.Tests.Views
{
    public class MovementViewTests
    {
        private MovementView viewer;

        private const float MOVING_TIME = 1f;
        private const float MOVING_SPEED = 5f;
        private const string LEVEL_LAYER_NAME = "Level";
        private static readonly int LEVEL_LAYER = LayerMask.NameToLayer(LEVEL_LAYER_NAME);

        [SetUp]
        public void Setup()
        {
            viewer = GameObjectTestUtility.InstantiateWithComponent<MovementView>(actived: false);
            viewer.model = InstantiateMovementModel();
            viewer.gameObject.SetActive(true);
        }

        [TearDown]
        public void Teardown()
        {
            Object.Destroy(viewer.model);
            Object.Destroy(viewer.gameObject);
        }

        [UnityTest]
        public IEnumerator WhenMoved_Position_ShouldRespectSpeedAndDirection()
        {
            var direction = Vector3.up;
            var originalPosition = viewer.transform.position;
            var expectedPosition = originalPosition + direction * MOVING_SPEED * Time.fixedDeltaTime;

            viewer.model.Speed = MOVING_SPEED;
            viewer.Move(direction);

            yield return new WaitForFixedUpdate();

            var actualPosition = viewer.transform.position;
            var positionsDistance = Vector3.Distance(actualPosition, expectedPosition);
            var isInExpectedPosition = positionsDistance <= Mathf.Epsilon;

            Assert.IsTrue(isInExpectedPosition, "Position is not in expected place.");
        }

        [UnityTest]
        public IEnumerator WhenMoved_Position_ShouldRespectVerticalSceneColliders()
        {
            var topCollider = InstantiateLevelCollider("TopCollider");
            var bottomCollider = InstantiateLevelCollider("BottomCollider");

            const float verticalDistance = 5F;
            topCollider.transform.position = viewer.transform.position + Vector3.up * verticalDistance;
            bottomCollider.transform.position = viewer.transform.position + Vector3.down * verticalDistance;

            viewer.model.Speed = MOVING_SPEED;
            viewer.Move(Vector3.up);
            yield return new WaitForSeconds(MOVING_TIME);

            var verticalTopColliderPos = topCollider.transform.position.y;
            var verticalBodyPos = viewer.transform.position.y;

            Assert.Less(verticalBodyPos, verticalTopColliderPos,
                "Body vertical position cannot be greater than vertical top collider position.");

            viewer.model.Speed = MOVING_SPEED * 2F;
            viewer.Move(Vector3.down);
            yield return new WaitForSeconds(MOVING_TIME);

            var verticalBottomColliderPos = bottomCollider.transform.position.y;
            verticalBodyPos = viewer.transform.position.y;

            Assert.Greater(verticalBodyPos, verticalBottomColliderPos,
                "Body vertical position cannot be less than vertical bottom collider position.");

            Object.Destroy(topCollider);
            Object.Destroy(bottomCollider);
        }

        [UnityTest]
        public IEnumerator WhenMoved_Position_ShouldRespectHorizontalSceneColliders()
        {
            var leftCollider = InstantiateLevelCollider("LeftCollider");
            var rightCollider = InstantiateLevelCollider("RightCollider");

            const float horizontalDistance = 5F;
            leftCollider.transform.position = viewer.transform.position + Vector3.left * horizontalDistance;
            rightCollider.transform.position = viewer.transform.position + Vector3.right * horizontalDistance;

            viewer.model.Speed = MOVING_SPEED;
            viewer.Move(Vector3.left);
            yield return new WaitForSeconds(MOVING_TIME);

            var horizontalLeftColliderPos = leftCollider.transform.position.x;
            var horizontalBodyPos = viewer.transform.position.x;

            Assert.Greater(horizontalBodyPos, horizontalLeftColliderPos,
                "Body horizontal position cannot be less than horizontal left collider position.");

            viewer.model.Speed = MOVING_SPEED * 2F;
            viewer.Move(Vector3.right);
            yield return new WaitForSeconds(MOVING_TIME);

            var horizontalRightColliderPos = rightCollider.transform.position.x;
            horizontalBodyPos = viewer.transform.position.x;

            Assert.Less(horizontalBodyPos, horizontalRightColliderPos,
                "Body horizontal position cannot be greater than horizontal right collider position.");

            Object.Destroy(leftCollider);
            Object.Destroy(rightCollider);
        }

        private static MovementModel InstantiateMovementModel()
        {
            var model = ScriptableObject.CreateInstance<MovementModel>();

            model.Speed = 0f;
            model.SpeedFactorMultiplier = 1f;
            model.InitialDirection = Vector2.zero;
            model.Collision = LayerMask.GetMask(LEVEL_LAYER_NAME);

            return model;
        }

        private static Collider2D InstantiateLevelCollider(string name)
        {
            var collider = GameObjectTestUtility.InstantiateWithComponent<BoxCollider2D>(name);
            collider.gameObject.layer = LEVEL_LAYER;
            return collider;
        }
    }
}
