﻿namespace Pong.Controllers
{
    /// <summary>
    /// Interface used on objects able to be displayed on screen.
    /// </summary>
    public interface IDisplayable
    {
        /// <summary>
        /// Shows the object.
        /// </summary>
        void Show();

        /// <summary>
        /// Hides the object.
        /// </summary>
        void Hide();
    }
}
