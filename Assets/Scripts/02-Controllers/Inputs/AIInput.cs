﻿using UnityEngine;

namespace Pong.Controllers
{
    /// <summary>
    /// Tracks the vertical position from the Ball GameObject and moves 
    /// a local component implementing <see cref="IMoveable"/> interface.
    /// </summary>
    [DisallowMultipleComponent]
    public sealed class AIInput : MonoBehaviour
    {
        [SerializeField, Tooltip("The tag from Ball GameObject")]
        private string ballTag = "Ball";

        private IMoveable moveable;
        private Transform ballTransform;

        private void Awake()
        {
            FindBallTransform();
            FindMoveableComponent();
        }


        private void Update() => TrackBallVerticalPosition();

        private void TrackBallVerticalPosition()
        {
            var cannotTrackBall = moveable == null || ballTransform == null;
            if (cannotTrackBall) return;

            var direction = (ballTransform.position - transform.position).normalized;
            direction.x = 0f;
            moveable.Move(direction);
        }

        private void FindBallTransform()
        {
            var ball = GameObject.FindGameObjectWithTag(ballTag);
            if (ball) ballTransform = ball.transform;
        }

        private void FindMoveableComponent()
            => moveable = GetComponent<IMoveable>();
    }
}