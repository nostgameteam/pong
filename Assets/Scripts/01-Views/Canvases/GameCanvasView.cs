﻿using UnityEngine;
using Pong.Controllers;

namespace Pong.Views
{
    /// <summary>
    /// Canvas view for the game.
    /// </summary>
    [DisallowMultipleComponent]
    public sealed class GameCanvasView : BaseCanvasView
    {
        [SerializeField, Tooltip("The child MenuCanvasView component.")]
        private MenuCanvasView menuCanvas;
        [SerializeField, Tooltip("The child CountdownCanvasView component.")]
        private CountdownCanvasView countdownCanvas;
        [SerializeField, Tooltip("The child ScoreCanvasView component.")]
        private ScoreCanvasView scoreCanvas;
        [SerializeField, Tooltip("The child MobilePlayerInput component.")]
        private MobilePlayerInput mobileInput;

        /// <summary>
        /// The child MenuCanvasView component.
        /// </summary>
        public MenuCanvasView MenuView => menuCanvas;

        /// <summary>
        /// The child CountdownCanvasView component.
        /// </summary>
        public CountdownCanvasView CountdownView => countdownCanvas;

        /// <summary>
        /// The child ScoreCanvasView component.
        /// </summary>
        public ScoreCanvasView ScoreView => scoreCanvas;

        /// <summary>
        /// Sets the given game controller for all children canvas.
        /// </summary>
        /// <param name="gameController"></param>
        public void SetGameController(GameMatchController gameController)
        {
            GameController = gameController;
            menuCanvas.GameController = gameController;
            countdownCanvas.GameController = gameController;
            scoreCanvas.GameController = gameController;
        }

        /// <summary>
        /// Shows only the start menu.
        /// </summary>
        public void ShowOnlyStartMenu()
        {
            menuCanvas.ShowStartMenu();
            countdownCanvas.Hide();
            scoreCanvas.Hide();
            mobileInput.Hide();
        }

        /// <summary>
        /// Shows only the replay menu.
        /// </summary>
        /// <param name="winningPaddle">Which paddle won.</param>
        public void ShowOnlyReplayMenu(PaddleView winningPaddle)
        {
            menuCanvas.ShowReplayMenu(winningPaddle);
            countdownCanvas.Hide();
            scoreCanvas.Hide();
            mobileInput.Hide();
        }

        /// <summary>
        /// Shows only the countdown canvas.
        /// </summary>
        public void ShowOnlyCountdown()
        {
            menuCanvas.Hide();
            countdownCanvas.Show();
            scoreCanvas.Hide();
            mobileInput.Hide();
        }

        /// <summary>
        /// Shows the gameplay canvas.
        /// <para>Mobile Input will be available if running on a mobile platform.</para>
        /// </summary>
        public void ShowGameplay()
        {
            menuCanvas.Hide();
            countdownCanvas.Hide();
            scoreCanvas.Show();
            if (IsRunningOnMobile()) mobileInput.Show();
        }

        protected override void FindComponents()
        {
            base.FindComponents();

            const bool includeInactive = true;
            menuCanvas = GetComponentInChildren<MenuCanvasView>(includeInactive);
            countdownCanvas = GetComponentInChildren<CountdownCanvasView>(includeInactive);
            scoreCanvas = GetComponentInChildren<ScoreCanvasView>(includeInactive);
            mobileInput = GetComponentInChildren<MobilePlayerInput>(includeInactive);
        }

        private static bool IsRunningOnMobile() => Application.isMobilePlatform;
    }
}
