﻿using UnityEngine;

namespace Pong.Controllers
{
    /// <summary>
    /// Interface used on objects able to have collisions.
    /// </summary>
    public interface ICollisionable
    {
        /// <summary>
        /// Updates the object position.
        /// </summary>
        /// <param name="position">The current position.</param>
        /// <param name="speed">The current speed to move.</param>
        /// <param name="collision">Collision mask to check collisions.</param>
        void UpdatePosition(Vector2 position, float speed, int collision);
    }
}