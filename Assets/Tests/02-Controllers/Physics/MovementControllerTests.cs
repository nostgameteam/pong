﻿using UnityEngine;
using NUnit.Framework;
using Pong.Controllers;

namespace Pong.Tests.Controllers
{
    public class MovementControllerTests
    {
        private MovementController controller;

        [SetUp]
        public void Setup()
        {
            var bounds = new Bounds(center: Vector2.zero, size: Vector2.one);
            controller = new MovementController(bounds, Vector2.zero);
        }

        [Test]
        public void WhenPositionIsUpdated_Position_ShouldMoveAccordingWithSpeedAndDirection()
        {
            const float speed = 5f;

            var direction = Vector3.up;
            var originalPosition = Vector3.zero;
            var expectedPosition = originalPosition + direction * speed * Time.deltaTime;

            controller.Direction = direction;
            controller.UpdatePosition(originalPosition, speed, collision: 0);

            var actualPosition = controller.Position;
            var isInExpectedPosition = Vector3.Distance(actualPosition, expectedPosition) < 0.001f;

            Assert.IsTrue(isInExpectedPosition, "CollisionController did not move to expected position after updated.");
        }
    }
}
