﻿namespace Pong.Models
{
    /// <summary>
    /// Enum for Paddle Side.
    /// </summary>
    public enum PaddleSideType
    {
        None,
        Left,
        Right
    }
}
