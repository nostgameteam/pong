﻿using UnityEngine;

namespace Pong.Controllers
{
    /// <summary>
    /// Interface used on objects able to have a 2D direction.
    /// </summary>
    public interface IDirectionable
    {
        /// <summary>
        /// The object 2D direction.
        /// </summary>
        Vector2 Direction { get; set; }
    }
}