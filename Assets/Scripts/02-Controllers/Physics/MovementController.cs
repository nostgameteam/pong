﻿using System;
using UnityEngine;

namespace Pong.Controllers
{
    /// <summary>
    /// Controller for movement using collision detection.
    /// </summary>
    public sealed class MovementController : ICollisionable, IPositionable, IDirectionable
    {
        #region Properties       
        /// <summary>
        /// The bounding area used to detect collisions.
        /// </summary>
        public Bounds Bounds { get; set; }

        /// <summary>
        /// The total size from bounding area used to detect collisions.
        /// </summary>
        public Vector2 Size => Bounds.size;

        /// <summary>
        /// The half-size from bounding area used to detect collisions.
        /// </summary>
        public Vector2 HalfSize => Bounds.extents;

        /// <summary>
        /// The current position.
        /// </summary>
        public Vector2 Position { get; private set; }

        /// <summary>
        /// The current direction.
        /// </summary>
        public Vector2 Direction { get; set; }
        #endregion

        private Action<RaycastHit2D> onCollision;

        /// <summary>
        /// Controller for movement using collision detection.
        /// </summary>
        /// <param name="bounds">A local bound instance.</param>
        /// <param name="bounds">The initial direction.</param>
        public MovementController(Bounds bounds, Vector2 direction)
        {
            Bounds = bounds;
            Direction = direction;
        }

        /// <summary>
        /// Updates the position, checking for collision and reacting to them along the way.
        /// </summary>
        /// <param name="position">The current position.</param>
        /// <param name="speed">The current speed to move.</param>
        /// <param name="collision">Collision mask to check collisions.</param>
        public void UpdatePosition(Vector2 position, float speed, int collision)
        {
            const float angle = 0f;

            var speedPerSecond = speed * Time.deltaTime;
            var hit = Physics2D.BoxCast(position, Bounds.size, angle, Direction, speedPerSecond, collision);
            var hasHit = hit.collider != null;
            var movingTowardsCollision = hasHit && Vector2.Dot(Direction, hit.normal) < 0f;

            if (movingTowardsCollision)
            {
                position = hit.point + hit.normal * HalfSize;
                onCollision?.Invoke(hit);
            }
            else
            {
                var velocityPerSecond = Direction * speedPerSecond;
                position += velocityPerSecond;
            }

            Position = position;
        }

        /// <summary>
        /// Registers a new collision event.
        /// </summary>
        /// <param name="onCollision">An event with <see cref="RaycastHit2D"/> parameter.</param>
        public void RegisterCollisionEvent(Action<RaycastHit2D> onCollision)
            => this.onCollision += onCollision;

        /// <summary>
        /// Unregisters an existent collision event.
        /// </summary>
        /// <param name="onCollision">An event with <see cref="RaycastHit2D"/> parameter.</param>
        public void UnregisterCollisionEvent(Action<RaycastHit2D> onCollision)
            => this.onCollision -= onCollision;
    }
}