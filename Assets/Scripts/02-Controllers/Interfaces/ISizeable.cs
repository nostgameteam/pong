﻿using UnityEngine;

namespace Pong.Controllers
{
    /// <summary>
    /// Interface used on objects able to have size.
    /// </summary>
    public interface ISizeable
    {
        /// <summary>
        /// The current object size.
        /// </summary>
    	Vector2 Size { get; }
    }
}