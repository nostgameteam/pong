﻿using Pong.Views;
using UnityEngine;

namespace Pong.Models
{
    /// <summary>
    /// Model class for a game match.
    /// </summary>
    [CreateAssetMenu(fileName = "NewGameMatchModel", menuName = "Pong/Game Match Model", order = 110)]
    public sealed class GameMatchModel : ScriptableObject
    {
        [Header("Countdown")]
        [SerializeField, Tooltip("The match countdown starts using this number.")]
        private int totalCountdown = 3;
        [SerializeField, Tooltip("Message shown after countdown ends.")]
        private string countdownFinalMessage = "GO";

        [Header("Score")]
        [SerializeField, Tooltip("The number of points gain per strike.")]
        private int pointsPerRound = 1;
        [SerializeField, Tooltip("The total points necessary to finish the match.")]
        private int pointsToFinish = 5;

        [Header("Gameplay")]
        [SerializeField, Tooltip("The ball prefab used on gameplay.")]
        private GameObject ballPrefab;
        [SerializeField, Tooltip("The left paddle prefab used on gameplay.")]
        private GameObject leftPaddlePrefab;
        [SerializeField, Tooltip("The second paddle prefab used on gameplay.")]
        private GameObject rightPaddlePrefab;
        [SerializeField, Tooltip("The Game Canvas UI prefab used on gameplay.")]
        private GameObject gameCanvasUI;

        [Header("Arena")]
        [SerializeField, Tooltip("The arena prefab used on gameplay.")]
        private GameObject arenaPrefab;

        /// <summary>
        /// The match countdown starts using this number.
        /// </summary>
        public int TotalCountdown => totalCountdown;

        /// <summary>
        /// Message shown after countdown ends.
        /// </summary>
        public string CountdownFinalMessage => countdownFinalMessage;

        /// <summary>
        /// The number of points gain per strike.
        /// </summary>
        public int PointsPerRound => pointsPerRound;

        /// <summary>
        /// The total points necessary to finish the match.
        /// </summary>
        public int PointsToFinish => pointsToFinish;

        /// <summary>
        /// Checks if the given total score is necessary to finish the match.
        /// </summary>
        /// <param name="totalScore"></param>
        /// <returns></returns>
        public bool HasFinishMatch(int totalScore) => totalScore >= PointsToFinish;

        /// <summary>
        /// Instantiates the ball prefab using the given position.
        /// </summary>
        /// <param name="position">A position in the world.</param>
        /// <returns></returns>
        public MovementView InstantiateBallPrefab(Vector2 position)
        {
            return
                Instantiate(ballPrefab, position, Quaternion.identity).
                GetComponent<MovementView>();
        }

        /// <summary>
        /// Instantiates the left paddle prefab using the given position.
        /// </summary>
        /// <param name="position">A position in the world.</param>
        /// <returns></returns>
        public PaddleView InstantiateLeftPaddlePrefab(Vector2 position)
        {
            var paddle =
                Instantiate(leftPaddlePrefab, position, Quaternion.identity).
                GetComponent<PaddleView>();
            paddle.PaddleSide = PaddleSideType.Left;
            return paddle;
        }

        /// <summary>
        /// Instantiates the right paddle prefab using the given position.
        /// </summary>
        /// <param name="position">A position in the world.</param>
        /// <returns></returns>
        public PaddleView InstantiateRightPaddlePrefab(Vector2 position)
        {
            var paddle =
                Instantiate(rightPaddlePrefab, position, Quaternion.identity).
                GetComponent<PaddleView>();
            paddle.PaddleSide = PaddleSideType.Right;
            return paddle;
        }

        /// <summary>
        /// Instantiates the game canvas prefab using the given position.
        /// </summary>
        /// <param name="position">A position in the world.</param>
        /// <returns></returns>
        public GameCanvasView InstantiateGameCanvasUIPrefab(Vector2 position)
        {
            return
                Instantiate(gameCanvasUI, position, Quaternion.identity).
                GetComponent<GameCanvasView>();
        }

        /// <summary>
        /// Instantiates the arena prefab using the given position.
        /// </summary>
        /// <param name="position">A position in the world.</param>
        /// <returns></returns>
        public ArenaView InstantiateArenaPrefab(Vector2 position)
        {
            return
                Instantiate(arenaPrefab, position, Quaternion.identity).
                GetComponent<ArenaView>();
        }
    }
}
