﻿using Pong.UI;
using UnityEngine;
using System;
using System.Collections;

namespace Pong.Views
{
    /// <summary>
    /// Canvas view for the countdown.
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(AudioSource))]
    public sealed class CountdownCanvasView : BaseCanvasView
    {
        [SerializeField, Tooltip("The local AudioSource component.")]
        private AudioSource audioSource;
        [SerializeField, Tooltip("The sound played when countdown ends.")]
        private AudioClip endSound;
        [SerializeField, Tooltip("The child NumericText component used for countdown.")]
        private NumericText countText;

        /// <summary>
        /// Plays the countdown sound.
        /// </summary>
        public void PlayCountdownSound() => audioSource.Play();

        /// <summary>
        /// Plays the countdown final sound.
        /// </summary>
        public void PlayCountdownFinalSound() => audioSource.PlayOneShot(endSound);

        /// <summary>
        /// Starts the countdown using the given params.
        /// </summary>
        /// <param name="totalCount">The countdown total number of digits.</param>
        /// <param name="finishText">The text displayed when countdown ends.</param>
        /// <param name="onFinish">The function executed when countdown ends.</param>
        public void StartCountdown(int totalCount, string finishText, Action onFinish)
            => StartCoroutine(StartCountdownCoroutine(totalCount, finishText, onFinish));

        private IEnumerator StartCountdownCoroutine(int totalCount, string finishText, Action onFinish)
        {
            var waitOneSecond = new WaitForSeconds(1F);

            for (int count = totalCount; count > 0; count--)
            {
                PlayCountdownSound();
                countText.Value = count;
                yield return waitOneSecond;
            }

            PlayCountdownFinalSound();
            countText.Text = $"<color=green>{finishText}</color>";
            yield return waitOneSecond;

            countText.Clear();
            onFinish.Invoke();
        }

        protected override void FindComponents()
        {
            base.FindComponents();
            audioSource = GetComponent<AudioSource>();
            countText = GetComponentInChildren<NumericText>();
        }
    }
}
