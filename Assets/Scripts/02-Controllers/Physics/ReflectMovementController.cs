﻿using UnityEngine;

namespace Pong.Controllers
{
    /// <summary>
    /// Controller to reflect movement when collisions are detected.
    /// </summary>
    public sealed class ReflectMovementController
    {
        /// <summary>
        /// The instance used for movement and collision detection.
        /// </summary>
        public MovementController MovementController { get; private set; }

        /// <summary>
        /// The instance used for movement and collision detection.
        /// </summary>
        public AudioSource AudioSource { get; private set; }

        /// <summary>
        /// Controller to reflect movement when collisions are detected.
        /// </summary>
        /// <param name="collisionController">The instance used for movement and collision detection.</param>
        public ReflectMovementController(MovementController collisionController)
        {
            MovementController = collisionController;
        }

        /// <summary>
        /// Controller to reflect movement when collisions are detected.
        /// </summary>
        /// <param name="collisionController">The instance used for movement and collision detection.</param>
        public ReflectMovementController(MovementController collisionController, AudioSource audioSource)
            : this(collisionController)
        {
            AudioSource = audioSource;
        }

        /// <summary>
        /// Registers a reflection event on entering into collisions.
        /// </summary>
        public void RegisterReflectEvent() => MovementController.RegisterCollisionEvent(ReflectOnHit);

        /// <summary>
        /// Unregisters a previous reflection event.
        /// </summary>
        public void UnregisterReflectEvent() => MovementController.UnregisterCollisionEvent(ReflectOnHit);

        /// <summary>
        /// Reflects the movement vertically or horizontally based on the hit direction.
        /// </summary>
        /// <param name="hit">A hit collision.</param>
        public void ReflectOnHit(RaycastHit2D hit)
        {
            const float diretionThreshold = 0.8F;

            var direction = (hit.point - MovementController.Position).normalized;
            var isVertiCollision = Mathf.Abs(direction.y) > diretionThreshold;
            var isHorizCollision = Mathf.Abs(direction.x) > diretionThreshold;

            if (isVertiCollision) ReflectVertically();
            else if (isHorizCollision) ReflectHorizontally();
        }

        /// <summary>
        /// Reflects the movement direction vertically.
        /// </summary>
        public void ReflectVertically()
        {
            var direction = MovementController.Direction;
            direction.y *= -1f;
            MovementController.Direction = direction;
            PlayAudio();
        }

        /// <summary>
        /// Reflects the movement direction horizontally.
        /// </summary>
        public void ReflectHorizontally()
        {
            var direction = MovementController.Direction;
            direction.x *= -1f;
            MovementController.Direction = direction;
            PlayAudio();
        }

        private void PlayAudio()
        {
            if (AudioSource) AudioSource.Play();
        }
    }
}
