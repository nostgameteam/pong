﻿namespace Pong.Models
{
    /// <summary>
    /// Enum for all possible match states.
    /// </summary>
    public enum MatchStateType
    {
        None,
        MainMenu,
        Countdown,
        Paused,
        Gameplay,
        FinishMenu
    }
}
