﻿using UnityEngine;

namespace Pong.Controllers
{
    /// <summary>
    /// Interface used on objects able to have a 2D position.
    /// </summary>
    public interface IPositionable
    {
        /// <summary>
        /// The object 2D position.
        /// </summary>
        Vector2 Position { get; }
    }
}