﻿using UnityEngine;

namespace Pong.Models
{
    /// <summary>
    /// Model class for movement.
    /// </summary>
    [CreateAssetMenu(fileName = "NewMovementModel", menuName = "Pong/Movement Model", order = 110)]
    public sealed class MovementModel : ScriptableObject
    {
        [SerializeField, Min(0f), Tooltip("The current speed to move.")]
        private float speed = 10f;
        [SerializeField, Min(0f), Tooltip("The speed factor used as multiplier.")]
        private float speedFactorMultiplier = 1f;
        [SerializeField, Tooltip("The mask to check for collisions.")]
        private LayerMask collision;
        [SerializeField, Tooltip("The initial direction to move.")]
        private Vector2 initialDirection = Vector2.one;

        /// <summary>
        /// The current speed to move.
        /// </summary>
        public float Speed
        {
            get => speed;
            set => speed = Mathf.Clamp(value, 0F, 100F);
        }

        /// <summary>
        /// The speed factor used as multiplier.
        /// </summary>
        public float SpeedFactorMultiplier
        {
            get => speedFactorMultiplier;
            set => speedFactorMultiplier = Mathf.Clamp(value, 0F, 10F);
        }

        /// <summary>
        /// The total speed used to move.
        /// </summary>
        public float TotalSpeed => Speed * SpeedFactorMultiplier;

        /// <summary>
        /// The mask to check for collisions.
        /// </summary>
        public LayerMask Collision
        {
            get => collision;
            set => collision = value;
        }

        /// <summary>
        /// The initial direction to move
        /// </summary>
        public Vector2 InitialDirection
        {
            get => initialDirection;
            set => initialDirection = value.normalized;
        }

        /// <summary>
        /// Initializes all attributes.
        /// </summary>
        /// <param name="speed">The current speed to move.</param>
        /// <param name="speedFactorMultiplier">The speed factor used as multiplier.</param>
        /// <param name="collision">The mask to check for collisions.</param>
        /// <param name="initialDirection">The initial direction to move.</param>
        public void Initialize(float speed, float speedFactorMultiplier, int collision,
            Vector2 initialDirection)
        {
            Speed = speed;
            SpeedFactorMultiplier = speedFactorMultiplier;
            Collision = collision;
            InitialDirection = initialDirection;
        }
    }
}