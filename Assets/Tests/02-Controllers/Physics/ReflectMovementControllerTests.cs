﻿using UnityEngine;
using NUnit.Framework;
using Pong.Controllers;

namespace Pong.Tests.Controllers
{
    public class ReflectMovementControllerTests
    {
        private ReflectMovementController controller;

        [SetUp]
        public void Setup()
        {
            var bounds = new Bounds(center: Vector2.zero, size: Vector2.one);
            var collisionController = new MovementController(bounds, direction: Vector2.zero);
            controller = new ReflectMovementController(collisionController);
        }

        [Test]
        public void WhenRefelectVertically_Direction_ShouldBeReflectedVertically()
        {
            var directionBeforeRefelect = controller.MovementController.Direction;

            controller.ReflectVertically();

            var directionAfterRefelect = controller.MovementController.Direction;
            var dotProduct = Vector2.Dot(directionBeforeRefelect, directionAfterRefelect);
            var isDirectionVerticallyReflected = Mathf.Approximately(dotProduct, 0F);

            Assert.That(isDirectionVerticallyReflected, "Direction is not vertically reflected.");
        }

        [Test]
        public void WhenRefelectHorizontally_Direction_ShouldBeReflectedHorizontally()
        {
            var directionBeforeRefelect = controller.MovementController.Direction;

            controller.ReflectHorizontally();

            var directionAfterRefelect = controller.MovementController.Direction;
            var dotProduct = Vector2.Dot(directionBeforeRefelect, directionAfterRefelect);
            var isDirectionHorizontallyReflected = Mathf.Approximately(dotProduct, 0F);

            Assert.That(isDirectionHorizontallyReflected, "Direction is not horizontally reflected.");
        }
    }
}
