﻿using UnityEngine;

namespace Pong.Tests
{
    /// <summary>
    /// Class utility for GameObjects.
    /// </summary>
    public static class GameObjectTestUtility
    {
        /// <summary>
        /// Instantiates a GameObject using the given name and component.
        /// </summary>
        /// <typeparam name="T">The component to instantiate with.</typeparam>
        /// <param name="name">GameObject instance name.</param>
        /// <param name="actived">Activates or deactivates the Game Object on creation.</param>
        /// <returns>A GameObject instance.</returns>
        public static T InstantiateWithComponent<T>(string name, bool actived = true) where T : Component
        {
            var go = new GameObject(name);
            go.SetActive(actived);
            var component = go.AddComponent<T>();
            return component;
        }

        /// <summary>
        /// Instantiates a GameObject using the given component as its name.
        /// </summary>
        /// <typeparam name="T">The component to instantiate with.</typeparam>
        /// <param name="actived">Activates or deactivates the GameObject on creation.</param>
        /// <returns>A GameObject instance.</returns>
        public static T InstantiateWithComponent<T>(bool actived = true) where T : MonoBehaviour
        {
            var name = typeof(T).Name;
            return InstantiateWithComponent<T>(name, actived);
        }
    }
}
