﻿using UnityEngine;
using Pong.Controllers;

namespace Pong.Views
{
    /// <summary>
    /// View component for <see cref="ReflectMovementController"/>.
    /// </summary>
    [DefaultExecutionOrder(1)]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(AudioSource))]
    [RequireComponent(typeof(MovementView))]
    public sealed class ReflectMovementView : MonoBehaviour
    {
        /// <summary>
        /// Controller to reflect objects when collisions are detected.
        /// </summary>
        public ReflectMovementController Controller { get; private set; }

        private AudioSource audioSource;
        private MovementView movementView;

        private void Awake()
        {
            SetupRequiredComponents();
            InitializeController();
        }

        private void OnEnable() => Controller.RegisterReflectEvent();

        private void OnDisable() => Controller.UnregisterReflectEvent();

        private void SetupRequiredComponents()
        {
            audioSource = GetComponent<AudioSource>();
            movementView = GetComponent<MovementView>();
        }

        private void InitializeController()
        {
            if (movementView == null) throw new System.NullReferenceException("MovementView instance is null.");
            else if (movementView.Controller == null)
            {
                throw new System.NullReferenceException("Controller for MovementView instance is null.");
            }

            Controller = new ReflectMovementController(movementView.Controller, audioSource);
        }
    }
}
