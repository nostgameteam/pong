﻿using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputAction;

namespace Pong.Controllers
{
    /// <summary>
    /// Receives inputs from a <see cref="InputActionReference"/> and moves 
    /// a local component implementing <see cref="IMoveable"/> interface.
    /// </summary>
    [DisallowMultipleComponent]
    public sealed class PlayerInput : MonoBehaviour
    {
        [SerializeField, Tooltip("The reference for Vertical Input.")]
        private InputActionReference moveInput;

        private IMoveable moveable;

        private void Awake() => BindVerticalMovement();

        private void OnEnable() => moveInput.action.Enable();

        private void OnDisable() => moveInput.action.Disable();

        public void Move(Vector2 direction) => moveable.Move(direction);

        private void BindVerticalMovement()
        {
            moveable = GetComponent<IMoveable>();
            var cannotBind = moveable == null || moveInput == null;
            if (cannotBind) return;

            void MoveVertically(CallbackContext ctx)
            {
                var verticalDirection = ctx.ReadValue<float>();
                var direction = Vector2.up * verticalDirection;
                Move(direction);
            }

            moveInput.action.started += MoveVertically;
            moveInput.action.canceled += MoveVertically;
        }
    }
}
