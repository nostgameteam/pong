﻿using UnityEngine;
using NUnit.Framework;
using Pong.Models;
using Pong.Views;
using UnityEngine.TestTools;
using System.Collections;

namespace Pong.Tests.Views
{
    public class GameMatchViewTests
    {
        private GameMatchView viewer;

        [SetUp]
        public void Setup()
        {
            viewer = GameObjectTestUtility.InstantiateWithComponent<GameMatchView>(actived: false);
            viewer.model = InstantiateGameMatchModel();
            viewer.gameObject.SetActive(true);
        }

        [TearDown]
        public void Teardown()
        {
            Object.Destroy(viewer.Controller.Arena.gameObject);
            Object.Destroy(viewer.Controller.Ball.gameObject);
            Object.Destroy(viewer.Controller.LeftPaddle.gameObject);
            Object.Destroy(viewer.Controller.RightPaddle.gameObject);
            Object.Destroy(viewer.Controller.GameView.gameObject);
            Object.Destroy(viewer.gameObject);
        }

        [Test]
        public void WhenGameStarts_CurrentState_ShouldBeInMainMenu()
        {
            var expectedState = MatchStateType.MainMenu;
            var currentState = viewer.Controller.CurrentState;

            Assert.AreEqual(expectedState, currentState, $"When starts, game should be in {expectedState} state");
        }

        [Test]
        public void WhenStartCountdown_CurrentState_ShouldBeInCountdownState()
        {
            viewer.Controller.StartCountdown();

            var expectedState = MatchStateType.Countdown;
            var currentState = viewer.Controller.CurrentState;
            Assert.AreEqual(expectedState, currentState, $"When countdown, game should be in {expectedState} state");
        }

        [UnityTest]
        public IEnumerator AfterCountdown_CurrentState_ShouldBeInGameplayState()
        {
            // There is the final "GO" message that counts one second in countdown ends.
            var finalCountdownMessageTime = 1.1F;
            var fullCountdown = viewer.model.TotalCountdown + finalCountdownMessageTime;

            viewer.Controller.StartCountdown();
            yield return new WaitForSeconds(fullCountdown);

            var expectedState = MatchStateType.Gameplay;
            var currentState = viewer.Controller.CurrentState;
            Assert.AreEqual(expectedState, currentState, $"After countdown, game should be in {expectedState} state");
        }

        [Test]
        public void WhenLeftPaddleScorePoints_LeftScore_ShouldBeIncreasedAccording()
        {
            var pointsToScore = viewer.model.PointsPerRound;
            var beforeScorePoints = viewer.Controller.GetLeftPaddleScore();

            viewer.Controller.ScorePointsFor(PaddleSideType.Left);

            var expectedScorePoints = beforeScorePoints + pointsToScore;
            var currentScorePoints = viewer.Controller.GetLeftPaddleScore();

            Assert.AreEqual(expectedScorePoints, currentScorePoints);
        }

        [Test]
        public void WhenRightPaddleScorePoints_RightScore_ShouldBeIncreasedAccording()
        {
            var pointsToScore = viewer.model.PointsPerRound;
            var beforeScorePoints = viewer.Controller.GetRightPaddleScore();

            viewer.Controller.ScorePointsFor(PaddleSideType.Right);

            var expectedScorePoints = beforeScorePoints + pointsToScore;
            var currentScorePoints = viewer.Controller.GetRightPaddleScore();

            Assert.AreEqual(expectedScorePoints, currentScorePoints);
        }

        private static GameMatchModel InstantiateGameMatchModel()
        {
            const string testMatchSettingsPath = "Settings/TestGameMatch";
            return Resources.Load<GameMatchModel>(testMatchSettingsPath);
        }
    }
}
