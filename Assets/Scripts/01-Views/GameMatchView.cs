﻿using UnityEngine;
using Pong.Models;
using Pong.Controllers;

namespace Pong.Views
{
    /// <summary>
    /// View component for <see cref="GameMatchController"/>.
    /// </summary>
    [DisallowMultipleComponent]
    public sealed class GameMatchView : MonoBehaviour
    {
        [Tooltip("The move model data used to feed the controller.")]
        public GameMatchModel model;

        /// <summary>
        /// Controller used to controls the game match.
        /// </summary>
        public GameMatchController Controller { get; private set; }

        private void Awake()
        {
            InitializeController();
            InitializeteMatch();
        }

        private void Update() => Controller?.UpdateGameplayState();

        private void InitializeController()
        {
            if (model == null) throw new System.NullReferenceException("Model was not set.");
            Controller = new GameMatchController(model, this);
        }

        private void InitializeteMatch()
        {
            Controller.InstantiatePrefabs();
            Controller.ShowMainMenu();
        }
    }
}
