﻿using Pong.Models;
using UnityEngine;

namespace Pong.Views
{
    /// <summary>
    /// View component for the Paddle.
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(MovementView))]
    [RequireComponent(typeof(SpriteRenderer))]
    public sealed class PaddleView : MonoBehaviour
    {
        /// <summary>
        /// The current Paddle color.
        /// </summary>
        public Color Color => Renderer.color;

        /// <summary>
        /// The current Paddle side.
        /// </summary>
        public PaddleSideType PaddleSide { get; set; }

        /// <summary>
        /// The local SpriteRenderer component used to renderer the paddle sprite.
        /// </summary>
        public SpriteRenderer Renderer { get; private set; }

        /// <summary>
        /// The local MovementView component used to move the paddle.
        /// </summary>
        public MovementView MovementView { get; private set; }

        private void Awake() => FindRequiredComponents();

        private void FindRequiredComponents()
        {
            Renderer = GetComponent<SpriteRenderer>();
            MovementView = GetComponent<MovementView>();
        }
    }
}
