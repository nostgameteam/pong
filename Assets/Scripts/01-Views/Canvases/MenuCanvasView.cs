﻿using UnityEngine;
using UnityEngine.UI;

namespace Pong.Views
{
    /// <summary>
    /// Canvas view for menu.
    /// </summary>
    [DisallowMultipleComponent]
    public sealed class MenuCanvasView : BaseCanvasView
    {
        [SerializeField, Tooltip("The child Text component used to display the winning paddle.")]
        private Text winningPaddleText;
        [SerializeField, Tooltip("The child Text component inside the Play Button.")]
        private Text playButtonText;

        /// <summary>
        /// Plays the game starting the countdown.
        /// </summary>
        public void PlayGame() => GameController.StartCountdown();

        /// <summary>
        /// Shows only the "Play" button.
        /// </summary>
        public void ShowStartMenu()
        {
            Show();
            ShowPlayButton();
            DisableWinningPaddleText();
        }

        /// <summary>
        /// Shows only the "Replay" button.
        /// </summary>
        /// <param name="winningPaddle">The winning player paddle.</param>
        public void ShowReplayMenu(PaddleView winningPaddle)
        {
            Show();
            ShowPlayAgainButton();
            ShowWinningPlayer(winningPaddle);
        }

        /// <summary>
        /// Shows the winning player message.
        /// </summary>
        /// <param name="winningPaddle">The winning player paddle.</param>
        public void ShowWinningPlayer(PaddleView winningPaddle)
        {
            EnableWinningPaddleText();
            var htmlColor = ColorUtility.ToHtmlStringRGB(winningPaddle.Color);
            winningPaddleText.text = $"<color=#{htmlColor}>{winningPaddle.PaddleSide}</color> PADDLE WON!";
        }

        private void ShowPlayButton()
        {
            const string playText = "PLAY";
            playButtonText.text = playText;
        }

        private void ShowPlayAgainButton()
        {
            const string playAgainText = "PLAY AGAIN";
            playButtonText.text = playAgainText;
        }

        private void EnableWinningPaddleText() => winningPaddleText.enabled = true;

        private void DisableWinningPaddleText() => winningPaddleText.enabled = false;

        protected override void FindComponents()
        {
            base.FindComponents();
            var winningPaddle = transform.Find("WinningPaddle");
            var playButton = transform.Find("PlayButton");
            if (playButton) playButtonText = playButton.GetComponentInChildren<Text>();
            if (winningPaddle)
            {
                winningPaddleText = winningPaddle.GetComponent<Text>();
                winningPaddleText.supportRichText = true;
            }
        }
    }
}
