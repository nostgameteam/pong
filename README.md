# Pong

This is a Unity project for a Pong game.

* Unity version: **2019.4**
* Current version: **1.0.0**

![Starting the game](/Documentation/showcase.gif)

## Latest Builds

1. Play the WebGL build [here](https://simmer.io/@hyagogow/pong).
2. Download the Android version [here](https://drive.google.com/u/0/uc?export=download&confirm=Vihv&id=1_9zr0_kBkkRWvPh8TPDM0yQkSYzTZj-1).

![Download android build](/Documentation/download-game-apk.jpg)

## How to Use

1. Clone the repo and open it with any **Unity 2019.4 LTS** version. 
2. The main Scene is the [Game](/Assets/Scenes/Game.unity) but you can open anyone since all are playable.

## Game Features

1. Main menu with a play button.
2. Countdown is displayed before the match starts.
3. Left Paddle is controlled by the player using W, S, Up arrow or Down arrow keys for keyboard and the DPAD for gamepads.
4. Right Paddle is controlled by an AI tracking the ball's Y position at a slower speed.
5. A message indicating which paddle won will be displayed when the match ends.
6. [Unit test code are available](/Assets/Tests/).
7. Scripts following the **MVC** patter.
8. You can change the game by setting the ScriptableObjects inside the [Gameplay](/Assets/Settings/Gameplay/) folder.

![Gameplay Script Objects](/Documentation/gameplay-script-objects.jpg)


## Disclaimer

Audio Clips from [Kalmalyzer Pong](https://github.com/Kalmalyzer/Pong/tree/master/Assets/Audio)

## Known Bugs

1. Audio clip [ball-reflect](/Assets/Audio/ball-reflect.ogg) is not playable on WebGL builds since it is too small.

---

**Hyago Oliveira**

[BitBucket](https://bitbucket.org/HyagoGow/) -
[Gamejolt](https://gamejolt.com/@nostgames/games) -
<hyagogow@gmail.com>