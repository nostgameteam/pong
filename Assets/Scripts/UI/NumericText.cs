﻿using UnityEngine;
using UnityEngine.UI;

namespace Pong.UI
{
    /// <summary>
    /// Draws numeric values into screen.
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Text))]
    public sealed class NumericText : MonoBehaviour
    {
        [SerializeField, Tooltip("The local Text component.")]
        private Text textUI;

        /// <summary>
        /// The current numeric value.
        /// </summary>
        public int Value
        {
            get => value;
            set
            {
                this.value = value;
                Text = this.value.ToString();
            }
        }

        /// <summary>
        /// The current text value.
        /// </summary>
        public string Text
        {
            get => textUI.text;
            set => textUI.text = value;
        }

        /// <summary>
        /// The text color.
        /// </summary>
        public Color Color
        {
            get => textUI.color;
            set => textUI.color = value;
        }

        private int value = 0;

        private void Reset() => FindTextComponent();

        /// <summary>
        /// Clears the text. 
        /// </summary>
        public void Clear()
        {
            value = 0;
            Text = string.Empty;
        }

        /// <summary>
        /// Finds the local Text component. 
        /// </summary>
        public void FindTextComponent() => textUI = GetComponent<Text>();
    }
}