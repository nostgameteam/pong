﻿using Pong.UI;
using Pong.Models;
using UnityEngine;

namespace Pong.Views
{
    /// <summary>
    /// Canvas view for score.
    /// </summary>
    [DisallowMultipleComponent]
    public sealed class ScoreCanvasView : BaseCanvasView
    {
        [SerializeField, Tooltip("Score for left paddle.")]
        private NumericText leftPaddleScore;
        [SerializeField, Tooltip("Score for right paddle.")]
        private NumericText rightPaddleScore;

        /// <summary>
        /// Returns the current left paddle score.
        /// </summary>
        /// <returns></returns>
        public int GetLeftPaddleScore() => leftPaddleScore.Value;

        /// <summary>
        /// Returns the current right paddle score.
        /// </summary>
        /// <returns></returns>
        public int GetRightPaddleScore() => rightPaddleScore.Value;

        /// <summary>
        /// Increases the score for the given paddle.
        /// </summary>
        /// <param name="paddleSide">The paddle to increase points.</param>
        /// <param name="points">The points to increase.</param>
        public void IncreasePaddleScore(PaddleSideType paddleSide, int points)
        {
            switch (paddleSide)
            {
                case PaddleSideType.Left:
                    IncreaseLeftPaddleScore(points);
                    break;
                case PaddleSideType.Right:
                    IncreaseRightPaddleScore(points);
                    break;
            }
        }

        /// <summary>
        /// Increases the score for the left paddle.
        /// </summary>
        /// <param name="points">The points to increase.</param>
        public void IncreaseLeftPaddleScore(int points) => leftPaddleScore.Value += points;

        /// <summary>
        /// Increases the score for the right paddle.
        /// </summary>
        /// <param name="points">The points to increase.</param>
        public void IncreaseRightPaddleScore(int points) => rightPaddleScore.Value += points;

        /// <summary>
        /// Clears the score for both paddles.
        /// </summary>
        public void ClearScore()
        {
            leftPaddleScore.Value = 0;
            rightPaddleScore.Value = 0;
        }

        /// <summary>
        /// Changes the color for the left paddle.
        /// </summary>
        /// <param name="color">The new color.</param>
        public void SetLeftPaddleScoreColor(Color color) => leftPaddleScore.Color = color;

        /// <summary>
        /// Changes the color for the right paddle.
        /// </summary>
        /// <param name="color">The new color.</param>
        public void SetRightPaddleScoreColor(Color color) => rightPaddleScore.Color = color;

        protected override void FindComponents()
        {
            base.FindComponents();
            var leftScore = transform.Find("LeftScoreText");
            var rightScore = transform.Find("RightScoreText");

            if (leftScore) leftPaddleScore = leftScore.GetComponent<NumericText>();
            if (rightScore) rightPaddleScore = rightScore.GetComponent<NumericText>();
        }
    }
}
