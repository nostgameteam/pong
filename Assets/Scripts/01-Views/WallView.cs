﻿using UnityEngine;
using Pong.Controllers;

namespace Pong.Views
{
    /// <summary>
    /// Viewer component for arena wall.
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(SpriteRenderer))]
    public sealed class WallView : MonoBehaviour, ISizeable
    {
        [SerializeField, Tooltip("The local BoxCollider2D component.")]
        private BoxCollider2D boxCollider;
        [SerializeField, Tooltip("The local SpriteRenderer component.")]
        private SpriteRenderer spriteRenderer;

        /// <summary>
        /// The wall size.
        /// </summary>
        public Vector2 Size
        {
            get => boxCollider.size;
            set
            {
                boxCollider.size = value;
                spriteRenderer.size = value;
            }
        }

        private void Reset()
        {
            FindRequiredComponents();
            SetupComponents();
        }

        private void FindRequiredComponents()
        {
            boxCollider = GetComponent<BoxCollider2D>();
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        private void SetupComponents()
        {
            boxCollider.isTrigger = false;
            spriteRenderer.drawMode = SpriteDrawMode.Tiled;
            spriteRenderer.tileMode = SpriteTileMode.Continuous;
        }
    }
}
