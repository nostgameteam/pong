﻿using Pong.Views;
using Pong.Models;
using UnityEngine;
using System.Collections;

namespace Pong.Controllers
{
    /// <summary>
    /// The game match controller.
    /// </summary>
    public sealed class GameMatchController
    {
        /// <summary>
        /// The instantiated arena.
        /// </summary>
        public ArenaView Arena { get; private set; }

        /// <summary>
        /// The instantiated ball.
        /// </summary>
        public MovementView Ball { get; private set; }

        /// <summary>
        /// The instantiated left paddle.
        /// </summary>
        public PaddleView LeftPaddle { get; private set; }

        /// <summary>
        /// The instantiated right paddle.
        /// </summary>
        public PaddleView RightPaddle { get; private set; }

        /// <summary>
        /// The instantiated game canvas.
        /// </summary>
        public GameCanvasView GameView { get; private set; }

        /// <summary>
        /// The current game match state.
        /// </summary>
        public MatchStateType CurrentState { get; private set; }

        private readonly GameMatchModel model;
        private readonly MonoBehaviour monobehaviour;

        /// <summary>
        /// Instantiates a game match controller using the given params.
        /// </summary>
        /// <param name="model">The model for this controller.</param>
        /// <param name="monobehaviour">A instance to start coroutines.</param>
        public GameMatchController(GameMatchModel model, MonoBehaviour monobehaviour)
        {
            this.model = model;
            this.monobehaviour = monobehaviour;
            CurrentState = MatchStateType.None;
        }

        #region Instantiation Methods
        /// <summary>
        /// Instantiates all prefabs necessary for a match.
        /// </summary>
        public void InstantiatePrefabs()
        {
            var arenaArea = GetArenaArea(Camera.main);
            InstantiateGameUI();
            InstantiateArenaPrefab(arenaArea);
            InstantiateGameplayPrefabs(arenaArea);
        }

        /// <summary>
        /// Instantiates only the arena prefab using the given area.
        /// </summary>
        /// <param name="arenaArea">The area rectangle to be used in the arena.</param>
        public void InstantiateArenaPrefab(Rect arenaArea)
        {
            var initialPosition = Vector2.zero;
            Arena = model.InstantiateArenaPrefab(initialPosition);
            Arena.Size = arenaArea.size;
        }

        /// <summary>
        /// Instantiates all gameplay prefabs using the given area.
        /// </summary>
        /// <param name="arenaArea">The area rectangle to be used to build the arena.</param>
        public void InstantiateGameplayPrefabs(Rect arenaArea)
        {
            var halfArenaSize = arenaArea.size * 0.5f;
            var initialBallPosition = Vector2.zero;
            var leftPaddlePosition = Vector2.left * halfArenaSize.x + Vector2.right;
            var rightPaddlePosition = Vector2.right * halfArenaSize.x + Vector2.left;

            Ball = model.InstantiateBallPrefab(initialBallPosition);
            LeftPaddle = model.InstantiateLeftPaddlePrefab(leftPaddlePosition);
            RightPaddle = model.InstantiateRightPaddlePrefab(rightPaddlePosition);

            GameView.ScoreView.SetLeftPaddleScoreColor(LeftPaddle.Color);
            GameView.ScoreView.SetRightPaddleScoreColor(RightPaddle.Color);
        }

        /// <summary>
        /// Instantiates only the game GUI prefab.
        /// </summary>
        public void InstantiateGameUI()
        {
            var position = Vector2.zero;
            GameView = model.InstantiateGameCanvasUIPrefab(position);
            GameView.SetGameController(this);
        }
        #endregion

        #region Go to Game States
        /// <summary>
        /// Shows the main menu with the "Play" button.
        /// </summary>
        public void ShowMainMenu()
        {
            PauseGameplay();
            GameView.ShowOnlyStartMenu();
            CurrentState = MatchStateType.MainMenu;
        }

        /// <summary>
        /// Starts the countdown before goes to the gameplay.
        /// </summary>
        public void StartCountdown()
        {
            ResetPositions();
            GameView.ShowOnlyCountdown();
            GameView.CountdownView.StartCountdown(
                totalCount: model.TotalCountdown,
                finishText: model.CountdownFinalMessage,
                onFinish: ResumeGameplay);
            CurrentState = MatchStateType.Countdown;
        }

        /// <summary>
        /// Resumes gameplay from a Paused state.
        /// </summary>
        public void ResumeGameplay()
        {
            GameView.ShowGameplay();
            Ball.Resume();
            LeftPaddle.MovementView.Resume();
            RightPaddle.MovementView.Resume();
            CurrentState = MatchStateType.Gameplay;
        }

        /// <summary>
        /// Finishes the gameplay with the given winning paddle.
        /// </summary>
        /// <param name="winningPaddle">The winning player paddle.</param>
        public void FinishGameplay(PaddleView winningPaddle)
        {
            PauseGameplay();
            ClearScore();
            GameView.ShowOnlyReplayMenu(winningPaddle);
            CurrentState = MatchStateType.FinishMenu;
        }

        /// <summary>
        /// Pauses the gameplay.
        /// </summary>
        public void PauseGameplay()
        {
            Ball.Pause();
            LeftPaddle.MovementView.Pause();
            RightPaddle.MovementView.Pause();
            CurrentState = MatchStateType.Paused;
        }

        /// <summary>
        /// Updates the gameplay state checking if the Ball has reached any goal.
        /// </summary>
        public void UpdateGameplayState()
        {
            if (CurrentState == MatchStateType.Gameplay)
            {
                CheckReachedGoal();
            }
        }
        #endregion

        /// <summary>
        /// Scores points for the given paddle.
        /// </summary>
        /// <param name="paddleSide">The player paddle side to receive points.</param>
        public void ScorePointsFor(PaddleSideType paddleSide)
            => GameView.ScoreView.IncreasePaddleScore(paddleSide, points: model.PointsPerRound);

        /// <summary>
        /// Returns current left paddle score.
        /// </summary>
        /// <returns></returns>
        public int GetLeftPaddleScore() => GameView.ScoreView.GetLeftPaddleScore();

        /// <summary>
        /// Returns current right paddle score.
        /// </summary>
        /// <returns></returns>
        public int GetRightPaddleScore() => GameView.ScoreView.GetRightPaddleScore();

        private void CheckReachedGoal()
        {
            var isLeftGoal = Ball.BottomLeftPosition.x > Arena.Area.xMax;
            var isRightGoal = Ball.TopRightPosition.x < Arena.Area.xMin;
            var paddleSide = PaddleSideType.None;

            if (isLeftGoal) paddleSide = PaddleSideType.Left;
            else if (isRightGoal) paddleSide = PaddleSideType.Right;

            if (paddleSide != PaddleSideType.None)
            {
                ScorePointsFor(paddleSide);
                CheckForGameFinish();
            }
        }

        private void ResetPositions()
        {
            Ball.ResetPosition();
            LeftPaddle.MovementView.ResetPosition();
            RightPaddle.MovementView.ResetPosition();
        }

        private void ClearScore() => GameView.ScoreView.ClearScore();

        private void CheckForGameFinish()
        {
            var winningPaddle = GetWinningPaddle();
            var hasFinishMatch = winningPaddle != null;

            if (hasFinishMatch) FinishGameplay(winningPaddle);
            else RestartGameplay();
        }

        private PaddleView GetWinningPaddle()
        {
            var leftPaddleScore = GetLeftPaddleScore();
            var rightPaddleScore = GetRightPaddleScore();

            var hasLeftPaddleWon = model.HasFinishMatch(leftPaddleScore);
            var hasRightPaddleWon = model.HasFinishMatch(rightPaddleScore);

            if (hasLeftPaddleWon) return LeftPaddle;
            else if (hasRightPaddleWon) return RightPaddle;

            return null;
        }

        private void RestartGameplay()
            => monobehaviour.StartCoroutine(RestartGameplayCoroutine());

        private IEnumerator RestartGameplayCoroutine()
        {
            PauseGameplay();
            ResetPositions();

            yield return new WaitForSeconds(1F);

            Ball.Move(GetRandomDirection());
            ResumeGameplay();
        }

        private static Rect GetArenaArea(Camera camera)
        {
            if (camera == null) return Rect.MinMaxRect(-10f, -8F, 10F, 8F);

            var screenArea = Screen.safeArea;
            var topRightPosition = camera.ScreenToWorldPoint(screenArea.max);
            var bottomLeftPosition = camera.ScreenToWorldPoint(screenArea.min);
            var screenSize = topRightPosition - bottomLeftPosition;
            return new Rect(
                bottomLeftPosition.x, bottomLeftPosition.y,
                width: screenSize.x, height: screenSize.y);
        }

        private static Vector2 GetRandomDirection()
        {
            int RandomUnitInt()
            {
                var r = Random.Range(0f, 100f);
                return r < 50F ? -1 : 1;
            }
            return new Vector2(RandomUnitInt(), RandomUnitInt());
        }
    }
}
