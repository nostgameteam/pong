﻿using UnityEngine;
using Pong.Controllers;

namespace Pong.Views
{
    /// <summary>
    /// View for arena.
    /// </summary>
    [DisallowMultipleComponent]
    public sealed class ArenaView : MonoBehaviour, ISizeable
    {
        [SerializeField, Tooltip("The child WallView component fixed at the top.")]
        private WallView topWall;
        [SerializeField, Tooltip("The child WallView component fixed at the bottom.")]
        private WallView bottomWall;

        /// <summary>
        /// The arena total size.
        /// </summary>
        public Vector2 Size
        {
            get => size;
            set
            {
                size = value;
                area = new Rect(Vector2.zero, size)
                {
                    center = transform.position
                };
                ResizeWalls();
                RepositionWalls();
            }
        }

        /// <summary>
        /// The arena area.
        /// </summary>
        public Rect Area => area;

        private Rect area;
        private Vector2 size;

        private void Reset() => FindChildrenComponents();

        private void FindChildrenComponents()
        {
            var topTransform = transform.Find("WallTop");
            var bottomTransform = transform.Find("WallBottom");

            if (topTransform) topWall = topTransform.GetComponent<WallView>();
            if (bottomTransform) bottomWall = bottomTransform.GetComponent<WallView>();
        }

        private void ResizeWalls()
        {
            var horizontalSize = new Vector2(size.x, 1F);
            topWall.Size = horizontalSize;
            bottomWall.Size = horizontalSize;
        }

        private void RepositionWalls()
        {
            var halfHeight = size.y * 0.5f;
            topWall.transform.position = transform.position + Vector3.up * halfHeight;
            bottomWall.transform.position = transform.position + Vector3.down * halfHeight;
        }
    }
}
