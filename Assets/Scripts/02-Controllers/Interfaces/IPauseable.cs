﻿namespace Pong.Controllers
{
    /// <summary>
    /// Interface used on objects able to pause.
    /// </summary>
    public interface IPauseable
    {
        /// <summary>
        /// Pauses the object.
        /// </summary>
    	void Pause();

        /// <summary>
        /// Resumes the object.
        /// </summary>
        void Resume();
    }
}